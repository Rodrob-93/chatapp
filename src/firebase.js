import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';

var firebaseConfig = {
    apiKey: "AIzaSyACN0mvDKgzI-JmEwP0rJH4YPegHgkmXj8",
    authDomain: "chatapp-6bd7a.firebaseapp.com",
    databaseURL: "https://chatapp-6bd7a.firebaseio.com",
    projectId: "chatapp-6bd7a",
    storageBucket: "chatapp-6bd7a.appspot.com",
    messagingSenderId: "490584824480",
    appId: "1:490584824480:web:17453f163b2486e4"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  export default firebase;